module.exports = {
  purge: [
    "./index.html",
    "./src/**/*.js",
    "./src/**/*.jsx",
    "./src/**/*.ts",
    "./src/**/*.tsx",
  ],
  darkMode: false,
  theme: {
    extend: {
      minWidth: {
        120: "120px",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
