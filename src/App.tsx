import { useRoutes } from 'solid-app-router'
import { createSignal, Suspense } from 'solid-js'
import Footer from './components/UI/Footer'
import Header from './components/UI/Header'
import LoadingFallback from './components/UI/LoadingFallback'
import { createRoutes } from './hooks/createRoutes'

const App = () => {
  const [routes, setRoutes] = createSignal(createRoutes())
  const Routes = useRoutes(routes())

  return (
    <>
      <Header routes={routes()} />
      <main class="mx-3" role="main">
        <Suspense fallback={<LoadingFallback>Loading...</LoadingFallback>}>
          <Routes />
        </Suspense>
      </main>
      <Footer />
    </>
  )
}

export default App
