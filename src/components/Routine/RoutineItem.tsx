import { entries } from 'lodash/fp'
import { ComponentProps, For } from 'solid-js'
import { useNavigate } from 'solid-app-router'
import { CardTitle } from '../Typeography/Title'
import Card from '../UI/Card'

const Schedule = (props: ComponentProps<any>) => (
  <ul class="gap-3 grid grid-cols-4">
    <For each={entries(props.schedule)}>
      {([day, routine]) => (
        <li>
          <p class={'font-bold'}>{day}</p>
          <For each={routine}>{(exercise) => <p>{exercise.name}</p>}</For>
          <p>{routine.name}</p>
        </li>
      )}
    </For>
  </ul>
)

const RoutineItem = (props: ComponentProps<any>) => {
  const navigate = useNavigate()

  return (
    <Card onClick={() => navigate(`/${props.routine.id}`, { replace: true })}>
      <CardTitle>{props.routine.name}</CardTitle>
      <div class="px-4 w-full">
        <Schedule schedule={props.routine.schedule} />
      </div>
    </Card>
  )
}

export default RoutineItem
