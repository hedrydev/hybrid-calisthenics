import { ComponentProps, For } from 'solid-js'
import RoutineItem from './RoutineItem'

const RoutineList = (props: ComponentProps<any>) => (
  <For each={props.routines} fallback={<p>No routines</p>}>
    {(routine) => <RoutineItem routine={routine} />}
  </For>
)

export default RoutineList
