import { children, ComponentProps, splitProps } from 'solid-js'

const Card = (props: ComponentProps<any>) => {
  const [_class, _props] = splitProps(props, ['class'])
  const c = children(() => props.children)
  return (
    <div
      class={`border border-purple-300 flex flex-col items-center justify-center max-w-xl mx-auto my-10 px-2 py-6 shadow-lg ${_class}`}
      {..._props}>
      {c()}
    </div>
  )
}

export default Card
