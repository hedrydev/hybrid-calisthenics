import { children } from 'solid-js'
import { CardTitle } from '../Typeography/Title'
import Card from './Card'

const LoadingFallback = (props) => {
  const c = children(() => props.children)

  return (
    <Card class="mx-auto my-4 text-center">
      <CardTitle>Loading...</CardTitle>
      {c() ? c() : null}
    </Card>
  )
}

export default LoadingFallback
