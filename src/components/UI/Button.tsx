import { children, ComponentProps } from 'solid-js'

const Button = (props: ComponentProps<any>) => {
  const c = children(() => props.children)
  return (
    <button
      class={`border border-purple-500 bg-purple-600 hover:-translate-y-1 hover:shadow-lg min-w-120 py-2 rounded-md shadow-md text-center text-white text-xl transform ${props.class}`}
      onClick={props.onClick}>
      {c()}
    </button>
  )
}

export default Button
