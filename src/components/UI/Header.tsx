import { NavLink } from 'solid-app-router'
import { For } from 'solid-js'
import { MainTitle } from '../Typeography/Title'
import Button from './Button'

const Header = (props) => (
  <header class="m-auto text-center max-w-lg">
    <MainTitle class="mt-6">Hybrid Calisthenics</MainTitle>
    <nav class="flex flex-row items-center justify-evenly mt-6">
      <For each={props.routes}>
        {(route) =>
          route.showInHeader() ? (
            <NavLink href={route.path}>
              <Button class="bg-purple-500">{route.label}</Button>
            </NavLink>
          ) : null
        }
      </For>
    </nav>
  </header>
)

export default Header
