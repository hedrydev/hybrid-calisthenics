const Footer = (props) => (
  <footer class="bg-white border-t border-purple-600 bottom-0 mt-20 py-10 text-center w-full">
    <p>This is an unofficial app, created for my personal use.</p>
  </footer>
)

export default Footer
