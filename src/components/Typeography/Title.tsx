import { children } from 'solid-js'

export const MainTitle = (props) => {
  const c = children(() => props.children)
  return <h1 class={`font-bold pt-10 text-center text-3xl uppercase ${props.class}`}>{c()}</h1>
}

export const SectionTitle = (props) => {
  const c = children(() => props.children)
  return <h2 class="font-bold pt-10 text-center text-2xl uppercase">{c()}</h2>
}

export const CardTitle = (props) => {
  const c = children(() => props.children)
  return <h3 class="py-4 text-2xl text-center">{c()}</h3>
}
