import { eq, keys, last, negate, pipe, sortBy, startCase, toNumber } from 'lodash/fp'
import { ComponentProps, createEffect, createSignal } from 'solid-js'
import { CardTitle } from '../Typeography/Title'
import Card from '../UI/Card'
import { Proficiency } from './config'
import ProficiencyButton from './ProficiencyButton'
import { ExerciseVariant } from '../../types/Exercise'

const notEq = negate(eq)
const maxProficiency = pipe(
  keys,
  sortBy((a: number, b: number) => b - a),
  last,
  toNumber
)

const renderSection = (type: 'reps' | 'time' | 'note') => (exercise: ExerciseVariant) => {
  const { target } = exercise
  return target[type] ? (
    <p>
      {startCase(type)}: {target[type]}
    </p>
  ) : null
}
const renderReps = renderSection('reps')
const renderTime = renderSection('time')
const renderNote = renderSection('note')

const renderSections = (exercise: ExerciseVariant) => (
  <>
    <p>Sets: {exercise?.target?.sets ?? 0}</p>
    {renderReps(exercise)}
    {renderTime(exercise)}
    {renderNote(exercise)}
  </>
)

const ExerciseInfoItem = (props: ComponentProps<any>) => {
  const [proficiency, setProficiency] = createSignal(Proficiency.LEVEL_1)
  const [exercise, setExercise] = createSignal(props.levels[proficiency()])

  createEffect(() => setExercise(props.levels[proficiency()]))

  const decreaseProficiency = () => {
    if (notEq(Proficiency.LEVEL_1, proficiency())) {
      setProficiency((prev) => prev - 1)
    }
  }

  const increaseProficiency = () => {
    if (notEq(maxProficiency(props.levels), proficiency())) {
      setProficiency((prev) => prev + 1)
    }
  }

  return (
    <li>
      <Card>
        <CardTitle>{exercise().variation}</CardTitle>
        <div className="flex flex-row items-center justify-evenly mt-4 mb-2">
          <ProficiencyButton onClick={decreaseProficiency}>Easier</ProficiencyButton>
          <ProficiencyButton onClick={increaseProficiency}>Harder</ProficiencyButton>
        </div>
        <div className="text-center">{renderSections(exercise())}</div>
      </Card>
    </li>
  )
}

export default ExerciseInfoItem
