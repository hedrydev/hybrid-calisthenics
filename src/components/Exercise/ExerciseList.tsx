import { For } from 'solid-js'
import Card from '../UI/Card'
import { CardTitle } from '../Typeography/Title'
import { prop, sortBy, values } from 'lodash/fp'

const ExerciseList = props => {
  const sortedExercises = sortBy( prop( 'name' ), props.exercises )
  return <ul>
    <For each={ sortedExercises }>{ exercise => (
      <li>
        <Card>
          <CardTitle>{ exercise.name }</CardTitle>
          <For each={ values( exercise.levels ) }>{ level => (
            <li>{ level.variation }</li>
          ) }</For>
        </Card>
      </li> )
    }</For>
  </ul>
}

export default ExerciseList
