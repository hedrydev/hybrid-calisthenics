import { ComponentProps, For } from 'solid-js'
import ExerciseInfoItem from './ExerciseInfoItem'
import Card from '../UI/Card'
import { CardTitle } from '../Typeography/Title'

const ExerciseInfoList = (props: ComponentProps<any>) => (
  <ul>
    <For
      each={props.exercises}
      fallback={
        <Card>
          <CardTitle>Rest Day</CardTitle>
        </Card>
      }>
      {(exercise) => <ExerciseInfoItem size={props.size} {...exercise} />}
    </For>
  </ul>
)

export default ExerciseInfoList
