import { always, cond, propEq, startCase, T } from 'lodash/fp'
import { ComponentProps, createSignal } from 'solid-js'
import { createToggle } from '../../hooks/createToggle'

const repConfig = {
  min: 0,
  step: 1,
  type: 'number',
}
const timeConfig = {
  type: 'text',
}
const weightConfig = {
  min: 0,
  step: 1,
  type: 'number',
}
const renderSection = (name: string) => {
  const startCaseName = startCase(name)
  const testAttr = `by${startCaseName}`
  console.log(testAttr)
  return (props: ComponentProps<any>) =>
    cond([
      [
        propEq(testAttr, true),
        always(
          <div className="form__group">
            <label htmlFor={props.id + startCaseName}>{props.label}</label>
            <input
              id={props.id + startCaseName}
              name={props.id + startCaseName}
              {...props.inputConfig}
            />
            <p>Aim for {props?.targets[name]}</p>
          </div>
        ),
      ],
      [T, always(null)],
    ])(props)
}

const renderReps = renderSection('reps')
const renderTime = renderSection('time')
const renderWeight = renderSection('weight')

const ExerciseTrackerItem = (props: ComponentProps<any>) => {
  const [done, setDone] = createToggle(props.done)
  const [reps, setReps] = createSignal(null)
  const [time, setTime] = createSignal(null)
  const [weight, setWeight] = createSignal(null)

  return (
    <>
      {renderReps({
        ...props,
        label: 'Reps',
        value: reps(),
        onChange: setReps,
        inputConfig: repConfig,
      })}
      {renderWeight({
        ...props,
        label: 'Weight',
        value: weight(),
        onChange: setWeight,
        inputConfig: weightConfig,
      })}
      {renderTime({
        ...props,
        label: 'Time',
        value: time(),
        onChange: setTime,
        inputConfig: timeConfig,
      })}
      <div className="form__group">
        <input id="done" name="done" type="checkbox" onChange={() => setDone()} checked={done()} />
        <label htmlFor="done">Done</label>
      </div>
    </>
  )
}

export default ExerciseTrackerItem
