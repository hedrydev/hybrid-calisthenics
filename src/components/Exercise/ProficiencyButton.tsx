import Button from '../UI/Button'
import { children, ComponentProps } from 'solid-js'

const ProficiencyButton = (props: ComponentProps<any>) => {
  const c = children(() => props.children)
  return (
    <Button class="bg-purple-400 mx-4" onClick={props.onClick}>
      {c()}
    </Button>
  )
}

export default ProficiencyButton
