import { render } from 'solid-js/web'
import './css/main.css'
import { Router } from 'solid-app-router'

import App from './App'

render(
  () => (
    <Router>
      <App />
    </Router>
  ),
  document.getElementById('root')
)
