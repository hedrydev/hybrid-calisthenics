import App from './App'
import { render } from 'solid-testing-library'
import '@testing-library/jest-dom'

test('renders without crashing', () => {
  const app = render(App)
  expect(app.container).toBeTruthy()
})
