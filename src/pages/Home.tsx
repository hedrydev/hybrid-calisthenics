import { Link, useNavigate } from 'solid-app-router'
import { onMount } from 'solid-js'
import { SectionTitle } from '../components/Typeography/Title'

const Home = (props) => {
  const navigate = useNavigate()

  onMount(() => navigate('/routines', { replace: true }))

  return (
    <main role="main" class="text-center">
      <SectionTitle>Home</SectionTitle>
      <div class="mt-10">
        <Link href="/routines">Check out some Routines</Link>
      </div>
    </main>
  )
}
export default Home
