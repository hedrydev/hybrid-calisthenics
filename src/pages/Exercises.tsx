import ExerciseList from '../components/Exercise/ExerciseList'
import { SectionTitle } from '../components/Typeography/Title'
import { exercises } from '../data/dummy'

const Exercises = (props) => (
  <section>
    <SectionTitle>Exercises</SectionTitle>
    <ExerciseList exercises={exercises} />
  </section>
)

export default Exercises
