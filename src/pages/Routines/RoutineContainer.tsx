import { getDay } from 'date-fns'
import { find, pipe, prop, propEq, toNumber, values } from 'lodash/fp'
import { useParams } from 'solid-app-router'
import { ComponentProps, createEffect, createSignal, ErrorBoundary } from 'solid-js'
import ExerciseInfoList from '../../components/Exercise/ExerciseInfoList'
import { SectionTitle } from '../../components/Typeography/Title'
import Button from '../../components/UI/Button'
import { DAYS } from '../../constants/constant'
import { routines } from '../../data/dummy'
import { CardTitle } from './../../components/Typeography/Title'
import { Routine } from '../../types/Routine'

const getScheduleForDay = (day: number) => pipe(prop('schedule'), values, (arr) => arr[day])
const getRoutineById = (id: string | number) => (routines: Routine[]) =>
  find(propEq('id', toNumber(id)), routines)

const RoutineContainer = () => {
  const params = useParams()
  const routine = getRoutineById(params.id)(routines)
  const [date] = createSignal(new Date())
  const [day, setDay] = createSignal(getDay(date()))
  const [schedule, setSchedule] = createSignal(getScheduleForDay(day())(routine))

  createEffect(() => pipe(getScheduleForDay(day()), setSchedule)(routine))

  const prevDay = () => setDay((prev: Day) => (prev > 0 ? prev - 1 : 6))
  const nextDay = () => setDay((prev: Day) => (prev < 6 ? prev + 1 : 0))

  return (
    <ErrorBoundary fallback={<SectionTitle>Something went wrong</SectionTitle>}>
      <SectionTitle>{routine.name}</SectionTitle>
      <div class="flex flex-row justify-between max-w-lg mt-10 mx-auto">
        <Button onClick={prevDay}>Prev</Button>
        <CardTitle class={'text-center'}>{DAYS[day()]}</CardTitle>
        <Button onClick={nextDay}>Next</Button>
      </div>
      <ExerciseInfoList exercises={schedule()} />
    </ErrorBoundary>
  )
}

export default RoutineContainer
