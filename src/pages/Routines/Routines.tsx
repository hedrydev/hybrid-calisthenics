import { SectionTitle } from '../../components/Typeography/Title'
import { routines } from '../../data/dummy'
import RoutineList from '../../components/Routine/RoutineList'
import { ComponentProps } from 'solid-js'

const Routines = (props: ComponentProps<any>) => (
  <section>
    <SectionTitle>Routines</SectionTitle>
    <RoutineList routines={routines} />
  </section>
)

export default Routines
