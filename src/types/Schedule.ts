import {
  FRIDAY,
  MONDAY,
  SATURDAY,
  SUNDAY,
  THURSDAY,
  TUESDAY,
  WEDNESDAY,
} from '../constants/constant'
import { Exercise } from './Exercise'

export type Schedule = {
  [SUNDAY]: Exercise[]
  [MONDAY]: Exercise[]
  [TUESDAY]: Exercise[]
  [WEDNESDAY]: Exercise[]
  [THURSDAY]: Exercise[]
  [FRIDAY]: Exercise[]
  [SATURDAY]: Exercise[]
}
