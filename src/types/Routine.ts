import { Exercise } from './Exercise'
import { Schedule } from './Schedule'

export type Routine = {
  id: string | number
  name: string
  schedule: Schedule
}
