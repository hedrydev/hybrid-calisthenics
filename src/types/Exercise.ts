import { Proficiency } from '../components/Exercise/config'

export type ExerciseVariant = {
  variation: string
  target: {
    sets: number
    reps?: number
    time?: number
    weight?: number
    note?: string
  }
}

export type Exercise = {
  name: string
  levels: ExerciseVariant[]
}
