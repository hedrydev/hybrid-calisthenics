import { BRIDGES, LEG_RAISES, PULL_UPS, PUSH_UPS, SQUATS } from './exercises'
import { HAMPTONS } from './routines/hampton'
import { WORK_WEEK } from './routines/workWeek'
import { Exercise } from '../types/Exercise'
import { Routine } from '../types/Routine'

export const routines: Routine[] = [HAMPTONS, WORK_WEEK]
export const exercises: Exercise[] = [PUSH_UPS, LEG_RAISES, SQUATS, PULL_UPS, BRIDGES]
