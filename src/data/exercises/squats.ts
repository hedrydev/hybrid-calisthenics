import { Proficiency } from '../../components/Exercise/config'
import { Exercise } from '../../types/Exercise'

export const SQUATS: Exercise = {
  name: 'Squats',
  levels: {
    [Proficiency.LEVEL_1]: {
      variation: 'Jackknife Squats',
      target: {
        sets: 3,
        reps: 35,
      },
    },
    [Proficiency.LEVEL_2]: {
      variation: 'Assisted Squats',
      target: {
        sets: 3,
        reps: 30,
      },
    },
    [Proficiency.LEVEL_3]: {
      variation: 'Half Squats',
      target: {
        sets: 2,
        reps: 35,
      },
    },
    [Proficiency.LEVEL_4]: {
      variation: 'Full Squats',
      target: {
        sets: 2,
        reps: 50,
      },
    },
    [Proficiency.LEVEL_5]: {
      variation: 'Narrow Squats',
      target: {
        sets: 2,
        reps: 25,
      },
    },
    [Proficiency.LEVEL_6]: {
      variation: 'Side-Staggered Squats',
      target: {
        sets: 2,
        reps: 20,
        note: 'Reps each side',
      },
    },
    [Proficiency.LEVEL_7]: {
      variation: 'Staggered Squats',
      target: {
        sets: 2,
        reps: 20,
        note: 'Reps each side',
      },
    },
    [Proficiency.LEVEL_8]: {
      variation: 'Assisted One-Leg Squats',
      target: {
        sets: 2,
        reps: 9,
        note: 'Reps each side',
      },
    },
    [Proficiency.LEVEL_9]: {
      variation: 'One-Leg Chair Squats',
      target: {
        sets: 2,
        reps: 9,
        note: 'Reps each side',
      },
    },
    [Proficiency.LEVEL_10]: {
      variation: 'One-Leg Squats',
      target: {
        sets: 2,
        reps: 20,
        note: 'Reps each side',
      },
    },
  },
}
