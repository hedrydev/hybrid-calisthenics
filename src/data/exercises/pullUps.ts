import { Proficiency } from '../../components/Exercise/config'
import { Exercise } from '../../types/Exercise'

export const PULL_UPS: Exercise = {
  name: 'Pull-Ups',
  levels: {
    [Proficiency.LEVEL_1]: {
      variation: 'Wall Pull-Ups',
      target: {
        sets: 3,
        reps: 50,
      },
    },
    [Proficiency.LEVEL_2]: {
      variation: 'Horizontal Pullups (Chest Height)',
      target: {
        sets: 3,
        reps: 30,
      },
    },
    [Proficiency.LEVEL_3]: {
      variation: 'Horizontal Pullups (Hip Height)',
      target: {
        sets: 3,
        reps: 25,
      },
    },
    [Proficiency.LEVEL_4]: {
      variation: 'Jackknife Pullups',
      target: {
        sets: 3,
        reps: 25,
      },
    },
    [Proficiency.LEVEL_5]: {
      variation: 'Full Pullups/Chinups',
      target: {
        sets: 2,
        reps: 12,
      },
    },
    [Proficiency.LEVEL_6]: {
      variation: 'Narrow Pullups',
      target: {
        sets: 2,
        reps: 9,
      },
    },
    [Proficiency.LEVEL_7]: {
      variation: 'One-Hand Pullups',
      target: {
        sets: 2,
        reps: 9,
        note: 'Reps each side',
      },
    },
    [Proficiency.LEVEL_8]: {
      variation: 'Archer Pullups',
      target: {
        sets: 2,
        reps: 7,
        note: 'Reps each side',
      },
    },
    [Proficiency.LEVEL_9]: {
      variation: 'One-Arm Pullups',
      target: {
        sets: 2,
        reps: 20,
        note: 'Reps each side',
      },
    },
  },
}
