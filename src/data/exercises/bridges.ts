import { Proficiency } from '../../components/Exercise/config'
import { Exercise } from '../../types/Exercise'

export const BRIDGES: Exercise = {
  name: 'Bridges',
  levels: {
    [Proficiency.LEVEL_1]: {
      variation: 'Short Bridges',
      target: {
        sets: 3,
        reps: 50,
      },
    },
    [Proficiency.LEVEL_2]: {
      variation: 'Straight Bridges',
      target: {
        sets: 3,
        reps: 40,
      },
    },
    [Proficiency.LEVEL_3]: {
      variation: 'Raised Straight Bridges',
      target: {
        sets: 2,
        reps: 25,
      },
    },
    [Proficiency.LEVEL_4]: {
      variation: 'Head Bridges',
      target: {
        sets: 2,
        reps: 20,
      },
    },
    [Proficiency.LEVEL_5]: {
      variation: 'Full Bridges',
      target: {
        sets: 2,
        reps: 15,
      },
    },
    [Proficiency.LEVEL_6]: {
      variation: 'One-Leg Gecko Bridges',
      target: {
        sets: 2,
        reps: 9,
        note: 'Reps each side',
      },
    },
    [Proficiency.LEVEL_7]: {
      variation: 'Walking Bridges',
      target: {
        sets: 1,
        reps: 25,
        note: 'Reps are number of steps',
      },
    },
    [Proficiency.LEVEL_8]: {
      variation: 'Wall-Walking Bridges',
      target: {
        sets: 2,
        reps: 9,
      },
    },
    [Proficiency.LEVEL_9]: {
      variation: 'Stand-to-Stand Bridges',
      target: {
        sets: 2,
        reps: 20,
        note: 'Work the eccentric (negative) portion of this first. Descend into a bridge and stand up normally. After you can do this well, lean back onto objects of decreasing height (stairs work well) until you can finally lean back into this exercise and stand back up with the power of your core and legs.',
      },
    },
  },
}
