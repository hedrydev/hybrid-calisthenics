import { Proficiency } from '../../components/Exercise/config'
import { Exercise } from '../../types/Exercise'

export const LEG_RAISES: Exercise = {
  name: 'Leg Raises',
  levels: {
    [Proficiency.LEVEL_1]: {
      variation: 'Knee Tucks',
      target: {
        sets: 3,
        reps: 40,
      },
    },
    [Proficiency.LEVEL_2]: {
      variation: 'Knee Raises',
      target: {
        sets: 3,
        reps: 40,
      },
    },
    [Proficiency.LEVEL_3]: {
      variation: 'Bent Leg Raises',
      target: {
        sets: 3,
        reps: 35,
      },
    },
    [Proficiency.LEVEL_4]: {
      variation: 'Leg Raises',
      target: {
        sets: 2,
        reps: 25,
      },
    },
    [Proficiency.LEVEL_5]: {
      variation: 'Hanging Knee Raises',
      target: {
        sets: 2,
        reps: 20,
      },
    },
    [Proficiency.LEVEL_6]: {
      variation: 'Hanging Bent Leg Raises',
      target: {
        sets: 2,
        reps: 15,
      },
    },
    [Proficiency.LEVEL_7]: {
      variation: 'Hanging Leg Raises',
      target: {
        sets: 2,
        reps: 9,
      },
    },
    [Proficiency.LEVEL_8]: {
      variation: 'L-Hold',
      target: {
        sets: 1,
        time: 30,
      },
    },
    [Proficiency.LEVEL_9]: {
      variation: 'V-Hold',
      target: {
        sets: 1,
        time: 30,
      },
    },
    [Proficiency.LEVEL_10]: {
      variation: 'Hanging V-Hold',
      target: {
        sets: 1,
        reps: 60,
      },
    },
  },
}
