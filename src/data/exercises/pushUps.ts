import { Proficiency } from '../../components/Exercise/config'
import { Exercise } from '../../types/Exercise'

export const PUSH_UPS: Exercise = {
  name: 'Push-ups',
  levels: {
    [Proficiency.LEVEL_1]: {
      variation: 'Wall Push-Up',
      target: {
        sets: 3,
        reps: 50,
      },
    },
    [Proficiency.LEVEL_2]: {
      variation: 'Incline Push-Up',
      target: {
        sets: 3,
        reps: 40,
      },
    },
    [Proficiency.LEVEL_3]: {
      variation: 'Knee Push-Up',
      target: {
        sets: 2,
        reps: 25,
      },
    },
    [Proficiency.LEVEL_4]: {
      variation: 'Full Push-Up',
      target: {
        sets: 2,
        reps: 25,
      },
    },
    [Proficiency.LEVEL_5]: {
      variation: 'Narrow Push-Up',
      target: {
        sets: 2,
        reps: 20,
      },
    },
    [Proficiency.LEVEL_6]: {
      variation: 'Side-Staggered Push-Up',
      target: {
        sets: 2,
        reps: 15,
        note: 'Reps each side',
      },
    },
    [Proficiency.LEVEL_7]: {
      variation: 'Archer Push-Up',
      target: {
        sets: 2,
        reps: 9,
        note: 'Reps each side',
      },
    },
    [Proficiency.LEVEL_8]: {
      variation: 'Sliding One-Arm Push-Up',
      target: {
        sets: 2,
        reps: 9,
        note: 'Reps each side',
      },
    },
    [Proficiency.LEVEL_9]: {
      variation: 'One-Arm Push-Up (Conventional)',
      target: {
        sets: 2,
        reps: 9,
        note: 'Reps each side',
      },
    },
    [Proficiency.LEVEL_10]: {
      variation: 'One-Arm Push-Up (Feet Together)',
      target: {
        sets: 2,
        reps: 20,
        note: 'Reps each side',
      },
    },
  },
}
