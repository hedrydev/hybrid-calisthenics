import { nanoid } from 'nanoid'
import {
  FRIDAY,
  MONDAY,
  SATURDAY,
  SUNDAY,
  THURSDAY,
  TUESDAY,
  WEDNESDAY,
} from '../../constants/constant'
import { BRIDGES, LEG_RAISES, PULL_UPS, PUSH_UPS, SQUATS } from '../exercises'
import { Routine } from '../../types/Routine'

export const WORK_WEEK: Routine = {
  id: 2,
  name: 'Work Week',
  schedule: {
    [SUNDAY]: [],
    [MONDAY]: [PULL_UPS],
    [TUESDAY]: [BRIDGES],
    [WEDNESDAY]: [PUSH_UPS],
    [THURSDAY]: [LEG_RAISES],
    [FRIDAY]: [SQUATS],
    [SATURDAY]: [],
  },
}
