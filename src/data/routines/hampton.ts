import {
  FRIDAY,
  MONDAY,
  SATURDAY,
  SUNDAY,
  THURSDAY,
  TUESDAY,
  WEDNESDAY,
} from '../../constants/constant'
import { BRIDGES, LEG_RAISES, PULL_UPS, PUSH_UPS, SQUATS } from '../exercises'
import { Routine } from '../../types/Routine'

export const HAMPTONS: Routine = {
  id: 1,
  name: "Hampton's",
  schedule: {
    [SUNDAY]: [],
    [MONDAY]: [LEG_RAISES, PUSH_UPS],
    [TUESDAY]: [],
    [WEDNESDAY]: [PULL_UPS, SQUATS],
    [THURSDAY]: [],
    [FRIDAY]: [BRIDGES],
    [SATURDAY]: [],
  },
}
