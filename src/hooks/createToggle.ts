import { Accessor, createSignal } from 'solid-js'
import { isBoolean } from 'lodash/fp'

export const createToggle = (initial): [Accessor<boolean>, Function] => {
  const [value, setValue] = createSignal(initial ?? false)

  const toggleValue = (next?) => {
    isBoolean(next) ? setValue(next) : setValue((prev) => !prev)
  }

  return [value, toggleValue]
}
