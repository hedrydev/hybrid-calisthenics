import { stubTrue, stubFalse } from 'lodash/fp'
import { lazy } from 'solid-js'

export const createRoutes = () => [
  {
    label: 'Home',
    path: '/',
    component: lazy(() => import('../pages/Home.tsx')),
    showInHeader: stubFalse,
  },
  {
    label: 'Routines',
    path: '/routines',
    component: lazy(() => import('../pages/Routines/Routines.tsx')),
    showInHeader: stubTrue,
  },
  {
    label: 'Routine',
    path: '/:id',
    component: lazy(() => import('../pages/Routines/RoutineContainer.tsx')),
    showInHeader: stubFalse,
  },
  {
    label: 'Exercises',
    path: '/exercises',
    component: lazy(() => import('../pages/Exercises.tsx')),
    showInHeader: stubTrue,
  },
]
